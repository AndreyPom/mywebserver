### Установка пакетов для создания веб-сервера на базе Ubuntu 16.04 ###
* *(работоспособность в других версиях не тестировалась)*
* Проект полностью позаимствован у [Drobkov](https://bitbucket.org/Drobkov/userplus-webserver-ubuntu) и переработан под свои нужды
### Базовая установка ###

```
wget https://bitbucket.org/AndreyPom/mywebserver/raw/4d95b60820eb1a77027dbbb8f681357f56bb68be/setup.sh
sh setup.sh
```
### Содержимое setup.sh ###
```
#!/bin/bash
apt update && apt install git ansible -y
git clone https://AndreyPom@bitbucket.org/AndreyPom/mywebserver.git
rm -rf mywebserver/.git
MYSQLROOTPASS=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 9 | head -n 1)
sed -i "s/password/$MYSQLROOTPASS/" mywebserver/vars/main.yml
ansible-playbook -i  mywebserver/hosts mywebserver/start.yml
```
* Загрузка скрипта установки и его запуск
* Установка Ansible - системы управления конфигурациями (Python)
* Создание директории mywebserver с каталогами и файлами для управления веб-сервером