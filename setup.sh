#!/bin/bash
apt update && apt upgrade && apt install git ansible -y
git clone https://AndreyPom@bitbucket.org/AndreyPom/mywebserver.git
rm -rf mywebserver/.git
MYSQLROOTPASS=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 9 | head -n 1)
sed -i "s/password/$MYSQLROOTPASS/" mywebserver/vars/main.yml
ansible-playbook -i  mywebserver/hosts mywebserver/start.yml